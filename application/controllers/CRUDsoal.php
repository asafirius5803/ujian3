<?php
class CRUDsoal extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");

	}
	public function index()
	{
		$this->load->view('CRUDsoal');
	}
	public function xmat()
	{
		$this->load->view('Matematika/CRUD');
	}
	public function xbind()
	{
		$this->load->view('BIndonesia/CRUD');
	}
	public function xipa()
	{
		$this->load->view('IPA/CRUD');
	}
}