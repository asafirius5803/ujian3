<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CRUDuser extends CI_Controller {

	function __construct() 
	{
        parent::__construct();

        $this->load->model('Usermodel', 'model');
    }

	public function index()
	{
		$get = $this->model->get_username();

		$data['list'] = $get->result();
		$this->load->view('indexuser', $data);
	}

	public function form($id=null)
	{
		$data['get'] = !empty($id) ? $this->model->get_username(array('id'=>$id))->row() : null;

		$this->load->view('form', $data);
	}

	public function save()
	{
		$post = $this->input->post();

		$data = array(
				'id'=> $post["id"],
				'username' 	=> $post["username"],
				'password' 	=> $post["password"],
				'level' 	=> $post["level"]
			);

		if (!empty($post["id"])) {
			$this->model->update($data, array('id'=>$post["id"]));
		} 
		else {
			$this->model->save($data);
		}

		redirect(base_url('CRUDuser'));
	}

	public function delete($id=null)
	{
		$this->model->delete(array('id'=>$id));

		redirect(base_url('CRUDuser'));
	}
}
