<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil extends CI_Controller {

 public function __construct() {
 parent::__construct();
 $this->load->model('hasil_model');
 }

public function index() {
 $data = array( 'title' => 'Data Nilai',
 'user' => $this->hasil_model->listing());
 $this->load->view('hasil',$data);
 }

}

