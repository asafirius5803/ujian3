<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket1s extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket1smodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket1smodel->get_data();


  $this->load->view('IPA/IPA1', $result);

 }



}
