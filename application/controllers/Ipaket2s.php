<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket2s extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket2smodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket2smodel->get_data();


  $this->load->view('IPA/IPA2', $result);

 }



}
