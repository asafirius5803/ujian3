<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ipaket3s extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipaket3smodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipaket3smodel->get_data();


  $this->load->view('IPA/IPA3', $result);

 }



}
