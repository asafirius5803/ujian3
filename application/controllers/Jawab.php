<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Jawab extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('jawabmodel');

  $this->load->helper('url');

 }


 public function save(){

  $datapos = $this->input->post(); /*$_POST[]*/

  // var_dump($datapos); die('aa');

  $data = $this->jawabmodel->save_data($datapos);


  redirect( base_url() . 'Soal/bind/');

 }


 public function jawab()

 { 

  $result ['data'] = $this->jawabmodel->get_data();

  $this->load->view('Jawab',$result);

 }
}
