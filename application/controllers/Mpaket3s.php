<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Mpaket3s extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('mpaket3smodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->mpaket3smodel->get_data();


  $this->load->view('Matematika/Matematika3', $result);

 }



}
