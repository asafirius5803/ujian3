<?php
class Soal extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
	}
	public function index()
	{
		$this->load->view('soal');
	}
	public function mat()
	{
		$this->load->view('Matematika/Matematika');
	}
	public function bind()
	{
		$this->load->view('BIndonesia/BIndonesia');
	}
	public function ipa()
	{
		$this->load->view('IPA/IPA');
	}
}