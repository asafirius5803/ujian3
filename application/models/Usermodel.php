<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodel extends CI_Model
{
	function get_username($where=null)
	{

		if (!empty($where)) {
			$this->db->where($where);
		}

		return $this->db->get('user');
	}

	function save($post)
	{
		return $this->db->insert('user', $post);
	}

	function update($set, $where)
	{
		return $this->db->where($where)->update('user', $set);
	}

	function delete($where)
	{
		return $this->db->where($where)->delete('user');
	}
}