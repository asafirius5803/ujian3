<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
</head>
<body>
				<section class="content-header">
    	<h1>
    		
            <small></small>
        </h1>
        <ol class="breadcrumb">
        	<li><a href="#"><i class="fa fa-dashboard"></i>Halaman Admin</a></li>
            <li>				<?php
		$tanggal= mktime(date("m"),date("d"),date("Y"));
		echo "<b>".date("d-M-Y",$tanggal)."</b> ";
		date_default_timezone_set('Asia/Jakarta');
		$jam=date("H:i");
		echo ", Pukul : <b>". $jam." "."</b>";
		$a = date ("H");
		?></li>
        </ol>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
        <li><a href="<?php echo base_url(); ?>Login/logout" onclick="return confirm('Keluar..?');"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>

	</section>
	<font size="24">
		|<a href="<?php echo site_url('Hasil/') ?>">Hasil dan Peringkat</a><br>
		|<a href="<?php echo site_url('Soal/') ?>">Soal</a><br>
		|<a href="<?php echo site_url('CRUDsoal/') ?>">CRUD Soal</a><br>
		|<a href="<?php echo site_url('CRUDuser/') ?>">CRUD User</a><br>
		|<a href="<?php echo site_url('Login/logout') ?>">Logout</a><br>
	</font>
	<div align="center"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>
</body>
</html>