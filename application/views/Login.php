<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Aplikasi Ujian Online</title>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		
		<div class="container">

			<h3>Login</h3>
			<section class="content-header">
        <ol class="breadcrumb">
        	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Selamat Datang</li>
            <li>						<?php
		$tanggal= mktime(date("m"),date("d"),date("Y"));
		echo "<b>".date("d-M-Y", $tanggal)."</b> ";
		date_default_timezone_set('Asia/Jakarta');
		$jam=date("H:i");
		echo ", Pukul : <b>". $jam." "."</b>";
		$a = date ("H");
		?></li>
        </ol>
	</section>
			<hr>
			<form action="<?php echo base_url('Login/user')?>" method="POST" >
				<div class="form-group">
					<label for="cari">Username</label>
					<input type="text" class="form-control" id="username" name="username" placeholder="Username">
				</div>
				<div class="form-group">
					<label for="cari">Password</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Password">
				</div>
				<input class="btn btn-primary" type="submit" value="Login">
				<a href="<?php echo base_url().'CRUDuser/form';?>" class="btn btn-primary">Register</a>
			</form>
		</div>
		<div align="center"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>
	</body>
</html>
