<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>
<body>

<div id="container">
	<h1>Halaman CRUD User</h1>

	<div id="body">
		<h3>CRUD User</h3>
		
		<a href="<?php echo base_url('CRUDuser') ?>">List Data</a>

		<form action="<?php echo base_url('CRUDuser/save') ?>" method="post">
			<input type="hidden" name="id" value="<?php echo !empty($get) ? $get->id : '' ?>">

			<table>
				<tr>
					<td>Username</td>
					<td>
						<input type="text" placeholder="Username" name="username" value="<?php echo !empty($get) ? $get->username : '' ?>" autofocus>
					</td>
				</tr>
				<tr>
					<td>Password</td>
					<td>
						<input type="text" placeholder="Password" name="password" value="<?php echo !empty($get) ? $get->password : '' ?>">
					</td>
				</tr>
				<tr>
					<td>Level</td>
					<td>
						<input type="text" placeholder="Level" name="level" value="<?php echo !empty($get) ? $get->level : '' ?>">
					</td>
				</tr>
				<tr>
					<td><button type="submit">Simpan</button> 
					<a href="javascript:window.history.go(-1);">Kembali</a></p><br></td>
				</tr>
			</table>
		</form>
	</div> 
	<p class="footer">Keterangan Level: 1. Admin<br>2. Guru<br> 3. Siswa<br> 4. Orang tua </p>
</div>
<div align="center"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>
</body>
</html>