<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <title><?php echo $title ?></title>
 <style type="text/css">
 body {
 background-color: grey;
 font-family: Arial;
 }
 main {
 width: 80%;
 padding: 20px;
 background-color: white;
 min-height: 300px;
 border-radius: 5px;
 margin: 30px auto;
 }
 table {
 border-top: solid thin #000;
 border-collapse: collapse;
 }
 th, td {
 border-top: border-top: solid thin #000;
 padding: 6px 12px;
 }
 </style>
</head>

<body>
 <main>
 				<?php
		$tanggal= mktime(date("m"),date("d"),date("Y"));
		echo "| <b>".date("d-M-Y", $tanggal)."</b> ";
		date_default_timezone_set('Asia/Jakarta');
		$jam=date("H:i");
		echo ", Pukul : <b>". $jam." "."</b>";
		$a = date ("H");
		?> <br>
 <h1>Laporan Hasil Nilai dan Peringkat</h1>
 <table border="1" width="100%">
 <thead>
 <tr>
 <th>Nama</th>
 <th>Matematika</th>
 <th>Bahasa Indonesia</th>
 <th>IPA</th>
 <th>Peringkat</th>
 </tr>
 </thead>
 <tbody>
 <?php $i=1; foreach($user as $user) { ?>
 <tr>
 <td><?php echo $user->Nama ?></td>
 <td><?php echo $user->Matematika ?></td>
 <td><?php echo $user->Bahasa_Indonesia ?></td>
 <td><?php echo $user->IPA ?></td>
 <td><?php echo $user->Peringkat ?></td>
 </tr>
 <?php $i++; } ?>
 </tbody>
 </table>
 <p class="footer" align="right"><a href="javascript:window.history.go(-1);">Kembali</a></p>
 </main>
 <div align="center"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>
</body>
</html>