<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<h3>Welcome</h3>
			<hr>
<?php 
if(count($login)>0)
{
	if($login->level == 1)
	{
		$this->load->view('Admin/H_admin');
 
	}
 
	elseif($login->level == 2)
	{
		$this->load->view('Guru/H_guru');
	}
 
	elseif($login->level == 3)
	{
		$this->load->view('Siswa/H_siswa');
	}
 
	elseif($login->level == 4)
	{
		$this->load->view('Ortu/H_ortu');
	}
	else
	{
		echo "<p>Anda login sebagai <b>". $login->username . "</b> .Level belum di setting, kontak admin.</p>";
	}
}
else
{
 	echo "<p>username/password yang anda masukkan salah.</p>";
}
?>
		</div>
	</body>
</html>